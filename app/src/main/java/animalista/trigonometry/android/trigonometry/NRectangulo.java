package animalista.trigonometry.android.trigonometry;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class NRectangulo extends Fragment {

    static EditText NRet_A;
    static EditText NRet_B;
    static EditText NRet_C;
    static EditText NRet_Alpha;
    static EditText NRet_Beta;
    static EditText NRet_Delta;
   static ImageView NRigual;
    static DecimalFormat f = new DecimalFormat("#.###", new DecimalFormatSymbols(Locale.US));


    public NRectangulo(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.nrectangulo, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
            NRet_A = (EditText) getActivity().findViewById(R.id.NRet_A);
            NRet_B = (EditText) getActivity().findViewById(R.id.NRet_B);
            NRet_C = (EditText) getActivity().findViewById(R.id.NRet_C);
        NRet_Alpha = (EditText) getActivity().findViewById(R.id.NRet_Alpha);
         NRet_Beta = (EditText) getActivity().findViewById(R.id.NRet_Beta);
        NRet_Delta = (EditText) getActivity().findViewById(R.id.NRet_Delta);
        NRigual = (ImageView) getActivity().findViewById(R.id.igualNR);
        NRigual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    //todos los lados llenos
                if(!NRet_A.isEnabled()){
                    Clean();
                    NRigual.setImageResource(getImageId(NRectangulo.this.getActivity(), "ic_action_done"));

                }else if((!NRet_A.getText().toString().matches("") && !NRet_B.getText().toString().matches("") && !NRet_C.getText().toString().matches(""))
                    ||
                    //Dos lados y un angulo lleno
                    //A B y Alpha
                    (!NRet_A.getText().toString().matches("") && !NRet_B.getText().toString().matches("") && !NRet_Alpha.getText().toString().matches(""))
                    ||
                    //A C y Alpha
                    (!NRet_A.getText().toString().matches("") && !NRet_C.getText().toString().matches("") && !NRet_Alpha.getText().toString().matches(""))
                    ||
                    //B C y Alpha
                    (!NRet_B.getText().toString().matches("") && !NRet_C.getText().toString().matches("") && !NRet_Alpha.getText().toString().matches(""))
                    ||

                    //A B y BNReta
                    (!NRet_A.getText().toString().matches("") && !NRet_B.getText().toString().matches("") && !NRet_Beta.getText().toString().matches(""))
                    ||
                    //A C y BNReta
                    (!NRet_A.getText().toString().matches("") && !NRet_C.getText().toString().matches("") && !NRet_Beta.getText().toString().matches(""))
                    ||
                    //B C y BNReta
                    (!NRet_B.getText().toString().matches("") && !NRet_C.getText().toString().matches("") && !NRet_Beta.getText().toString().matches(""))
                    ||

                    //A B y Delta
                    (!NRet_A.getText().toString().matches("") && !NRet_B.getText().toString().matches("") && !NRet_Delta.getText().toString().matches(""))
                    ||
                    //A C y Delta
                    (!NRet_A.getText().toString().matches("") && !NRet_C.getText().toString().matches("") && !NRet_Delta.getText().toString().matches(""))
                    ||
                    //B C y Delta
                    (!NRet_B.getText().toString().matches("") && !NRet_C.getText().toString().matches("") && !NRet_Delta.getText().toString().matches(""))
                    ||
                    //Un lado, Dos angulos llenos
                    //Alpha BNReta y A
                    (!NRet_Alpha.getText().toString().matches("") && !NRet_Beta.getText().toString().matches("") && !NRet_A.getText().toString().matches(""))
                    ||
                    //Alpha Delta y A
                    (!NRet_Alpha.getText().toString().matches("") && !NRet_Delta.getText().toString().matches("") && !NRet_A.getText().toString().matches(""))
                    ||
                    //BNReta Delta y A
                    (!NRet_Beta.getText().toString().matches("") && !NRet_Delta.getText().toString().matches("") && !NRet_A.getText().toString().matches(""))
                    ||
                    //Alpha BNReta y B
                    (!NRet_Alpha.getText().toString().matches("") && !NRet_Beta.getText().toString().matches("") && !NRet_B.getText().toString().matches(""))
                    ||
                    //Alpha Delta y B
                    (!NRet_Alpha.getText().toString().matches("") && !NRet_Delta.getText().toString().matches("") && !NRet_B.getText().toString().matches(""))
                    ||
                    //BNReta Delta y B
                    (!NRet_Beta.getText().toString().matches("") && !NRet_Delta.getText().toString().matches("") && !NRet_B.getText().toString().matches(""))
                    ||
                    //Alpha BNReta y C
                    (!NRet_Alpha.getText().toString().matches("") && !NRet_Beta.getText().toString().matches("") && !NRet_C.getText().toString().matches(""))
                    ||
                    //Alpha Delta y C
                    (!NRet_Alpha.getText().toString().matches("") && !NRet_Delta.getText().toString().matches("") && !NRet_C.getText().toString().matches(""))
                    ||
                    //BNReta Delta y C
                    (!NRet_Beta.getText().toString().matches("") && !NRet_Delta.getText().toString().matches("") && !NRet_C.getText().toString().matches(""))
                    ){

                    String[] Valores = Resultado(NRet_A.getText().toString(), NRet_B.getText().toString(), NRet_C.getText().toString(),
                            NRet_Alpha.getText().toString(), NRet_Beta.getText().toString(), NRet_Delta.getText().toString());
                    System.out.println("A= " +Valores[0]);
                    System.out.println("B= " +Valores[1]);
                    System.out.println("C= " +Valores[2]);
                    System.out.println("Alpha= " +Valores[3]);
                    System.out.println("Beta= " +Valores[4]);
                    System.out.println("Delta= " +Valores[5]);

                    if(Double.isNaN(Double.parseDouble(Valores[0])) || Double.isNaN(Double.parseDouble(Valores[1]))
                    || Double.isNaN(Double.parseDouble(Valores[2])) || Double.isNaN(Double.parseDouble(Valores[3]))
                    || Double.isNaN(Double.parseDouble(Valores[4])) || Double.isNaN(Double.parseDouble(Valores[5]))
                    || Double.parseDouble(Valores[3]) + Double.parseDouble(Valores[4]) + Double.parseDouble(Valores[5]) != 180){
                        Toast.makeText(NRectangulo.this.getActivity(), "Triangle doesn't exist", Toast.LENGTH_SHORT).show();
                        Clean();
                    }else {

                        NRet_A.setText(Valores[0]);
                        NRet_A.setEnabled(false);
                        NRet_A.setTextColor(Color.BLACK);
                        NRet_A.setFocusable(false);

                        NRet_B.setText(Valores[1]);
                        NRet_B.setEnabled(false);
                        NRet_B.setTextColor(Color.BLACK);
                        NRet_B.setFocusable(false);

                        NRet_C.setText(Valores[2]);
                        NRet_C.setEnabled(false);
                        NRet_C.setTextColor(Color.BLACK);
                        NRet_C.setFocusable(false);

                        NRet_Alpha.setText(Valores[3] + "°");
                        NRet_Alpha.setEnabled(false);
                        NRet_Alpha.setTextColor(Color.BLACK);
                        NRet_Alpha.setFocusable(false);

                        NRet_Beta.setText(Valores[4] + "°");
                        NRet_Beta.setEnabled(false);
                        NRet_Beta.setTextColor(Color.BLACK);
                        NRet_Beta.setFocusable(false);

                        NRet_Delta.setText(Valores[5] + "°");
                        NRet_Delta.setEnabled(false);
                        NRet_Delta.setTextColor(Color.BLACK);
                        NRet_Delta.setFocusable(false);
                        NRigual.setImageResource(getImageId(NRectangulo.this.getActivity(), "ic_action_refresh"));
                    }
                }else{
                    Toast.makeText(NRectangulo.this.getActivity(), "Please check the data", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    public String[] Resultado(String NRet_A, String NRet_B, String NRet_C, String NRet_Alpha, String NRet_Beta, String NRet_Delta) {
        //  0       1       2       3           4           5
        String[] v = {NRet_A, NRet_B, NRet_C, NRet_Alpha, NRet_Beta, NRet_Delta};
        int lados = 0;
        int angulos = 0;
        Double ladoA, ladoB ,ladoC, Alpha, Beta, Delta;
        if (v[0].matches("")) {
            ladoA = 0D;
        } else{
            ladoA = Double.parseDouble(v[0]);
            lados+=1;
        }
        if (v[1].matches("")) {
            ladoB = 0D;
        } else{
            ladoB = Double.parseDouble(v[1]);
            lados+=1;
        }
        if (v[2].matches("")) {
            ladoC = 0D;
        } else{
            ladoC = Double.parseDouble(v[2]);
            lados+=1;
        }
        if (v[3].matches("")) {
            Alpha = 0D;
        } else{
            Alpha = Double.parseDouble(v[3]);
            angulos+=1;
        }
        if (v[4].matches("")) {
            Beta = 0D;
        } else{
            Beta = Double.parseDouble(v[4]);
            angulos+=1;
        }
        if (v[5].matches("")) {
            Delta = 0D;
        } else{
            Delta = Double.parseDouble(v[5]);
            angulos+=1;
        }
        System.out.println("lados: " + lados + "Angulos: " + angulos);

        if ((lados == 2) && (angulos == 1)){
                v = DosLadosUnAngulo(ladoA,ladoB,ladoC,Alpha,Beta,Delta);
            return v;

        }else if((lados == 1)&&(angulos == 2)){

                v = UnLadoDosAngulos(ladoA,ladoB,ladoC,Alpha,Beta,Delta);

            return v;
        }else if((lados == 3) && (angulos == 0)){
            v = TresLados(ladoA,ladoB,ladoC,Alpha,Beta,Delta);
            return v;
        }


        System.out.println("A . = " +v[0]);
        System.out.println("B . = " +v[1]);
        System.out.println("C . = " +v[2]);
        System.out.println("Alpha . = " +v[3]);
        System.out.println("Beta . = " +v[4]);
        System.out.println("Delta . = " +v[5]);



        return v;
    }
    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    public void Clean(){
        NRet_A.setText("");
        NRet_A.setEnabled(true);
        NRet_A.setClickable(true);
        NRet_A.setFocusable(true);
        NRet_A.setFocusableInTouchMode(true);

        NRet_B.setText("");
        NRet_B.setEnabled(true);
        NRet_B.setClickable(true);
        NRet_B.setFocusable(true);
        NRet_B.setFocusableInTouchMode(true);

        NRet_C.setText("");
        NRet_C.setEnabled(true);
        NRet_C.setClickable(true);
        NRet_C.setFocusable(true);
        NRet_C.setFocusableInTouchMode(true);

        NRet_Alpha.setText("");
        NRet_Alpha.setEnabled(true);
        NRet_Alpha.setClickable(true);
        NRet_Alpha.setFocusable(true);
        NRet_Alpha.setFocusableInTouchMode(true);

        NRet_Beta.setText("");
        NRet_Beta.setEnabled(true);
        NRet_Beta.setClickable(true);
        NRet_Beta.setFocusable(true);
        NRet_Beta.setFocusableInTouchMode(true);

        NRet_Delta.setText("");
        NRet_Delta.setEnabled(true);
        NRet_Delta.setClickable(true);
        NRet_Delta.setFocusable(true);
        NRet_Delta.setFocusableInTouchMode(true);
    }

    double round(double paramDouble){
        return Math.round(paramDouble * 10000000000.0D) / 10000000000.0D;
    }


    public String[] DosLadosUnAngulo(Double ladoA, Double ladoB, Double ladoC, Double Alpha, Double Beta, Double Delta){
        Double ac,bc,cc,senalpha,senbeta,sendelta;

       //Lado A vacio
        if (ladoA == 0D){
            //Alpha
            if(Alpha != 0D){
                //Alpha B y C
                senalpha  = round(Math.sin(Math.toRadians(Alpha)));
                bc = Math.pow(ladoB, 2);
                cc = Math.pow(ladoC,2);
                ladoA =round(Math.sqrt(bc+cc-2*ladoB*ladoC*Math.cos(Math.toRadians(Alpha))));
                Beta = round(Math.toDegrees(Math.asin(ladoB*senalpha/ladoA)));
                Delta = 180D - Alpha - Beta;
                return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
            }
            //Beta
            else if(Beta != 0D){
                //Beta B y C
                senbeta  = round(Math.sin(Math.toRadians(Beta)));
                Delta=round(Math.toDegrees(Math.asin(ladoC*senbeta/ladoB)));
                Alpha = 180D - Delta - Beta;
                senalpha  = round(Math.sin(Math.toRadians(Alpha)));
                sendelta  = round(Math.sin(Math.toRadians(Delta)));
                ladoA = round(ladoB*senalpha/senbeta);
                return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
            }
            //Delta
            else if(Delta != 0D){
                //Delta B y C
                sendelta  = round(Math.sin(Math.toRadians(Delta)));
                Beta=round(Math.toDegrees(Math.asin(ladoB*sendelta/ladoC)));
                Alpha = 180D - Beta - Delta;
                senalpha = round(Math.sin(Math.toRadians(Alpha)));
                senbeta  = round(Math.sin(Math.toRadians(Beta)));
                ladoA = round(ladoB*senalpha/senbeta);
                return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
            }
        }
        //Lado B vacio
        else if (ladoB == 0D){
            //Alpha
            if(Alpha != 0D){
                //Alpha A y C
                senalpha  = round(Math.sin(Math.toRadians(Alpha)));
                Delta = round(Math.toDegrees(Math.asin(ladoC*senalpha/ladoA)));
                Beta = 180D - Alpha - Delta;
                senbeta  = round(Math.sin(Math.toRadians(Beta)));
                sendelta = round(Math.sin(Math.toRadians(Delta)));
                ladoB = round(ladoA*senbeta/senalpha);
                return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
            }
            //Beta
            else if(Beta != 0D){
                //Beta A y C
                senbeta  = round(Math.sin(Math.toRadians(Beta)));
                ac = Math.pow(ladoA, 2);
                cc = Math.pow(ladoC, 2);
                ladoB = round(Math.sqrt(ac+cc-2*ladoA*ladoC*Math.cos(Math.toRadians(Beta))));
                Alpha = round(Math.toDegrees(Math.asin(ladoA*senbeta/ladoB)));
                Delta = 180D - Alpha - Beta;
                return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
            }
            //Delta
            else if(Delta != 0D){
                //Delta A y C
                sendelta  = round(Math.sin(Math.toRadians(Delta)));
                Alpha = round(Math.toDegrees(Math.asin(ladoA*sendelta/ladoC)));
                Beta = 180D - Alpha - Delta;
                senalpha = round(Math.sin(Math.toRadians(Alpha)));
                senbeta  = round(Math.sin(Math.toRadians(Beta)));
                ladoB = round(ladoC*senbeta/sendelta);
                return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
            }
        }
        //Lado C vacio
        else if (ladoC == 0D){
            //Alpha
            if(Alpha != 0D){
                //Alpha A y B
                senalpha  = round(Math.sin(Math.toRadians(Alpha)));
                Beta = round(Math.toDegrees(Math.asin(ladoB*senalpha/ladoA)));
                Delta = 180D - Beta - Alpha;
                senbeta  = round(Math.sin(Math.toRadians(Beta)));
                sendelta = round(Math.sin(Math.toRadians(Delta)));
                ladoC = round(ladoB*sendelta/senbeta);
                return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
            }
            //Beta
            else if(Beta != 0D){
                //Beta A y B
                senbeta  = round(Math.sin(Math.toRadians(Beta)));
                Alpha = round(Math.toDegrees(Math.asin(ladoA*senbeta/ladoB)));
                Delta = 180D - Alpha - Beta;
                senalpha  = round(Math.sin(Math.toRadians(Alpha)));
                sendelta  = round(Math.sin(Math.toRadians(Delta)));
                ladoC = round(ladoB*sendelta/senbeta);
                return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
            }
            //Delta
            else if(Delta != 0D){
                //Delta A y B
                sendelta  = round(Math.sin(Math.toRadians(Delta)));
                ac = Math.pow(ladoA, 2);
                bc = Math.pow(ladoB, 2);
                ladoC = round(Math.sqrt(ac+bc-2*ladoA*ladoB*Math.cos(Math.toRadians(Delta))));
                Beta = round(Math.toDegrees(Math.asin(ladoB*sendelta/ladoC)));
                Alpha = 180D - Beta - Delta;
                return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
            }
        }
        return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
    }

    public String[] UnLadoDosAngulos(Double ladoA, Double ladoB, Double ladoC, Double Alpha, Double Beta, Double Delta){
        Double senalpha,senbeta,sendelta;
        if (Alpha == 0){
            Alpha = 180D - Beta - Delta;
        }else if (Beta == 0){
            Beta = 180D - Alpha - Delta;
        }else if (Delta == 0){
            Delta = 180D - Alpha - Beta;
        }

        if (ladoA != 0){
            //A Alpha Beta Delta
            senbeta  = round(Math.sin(Math.toRadians(Beta)));
            senalpha = round(Math.sin(Math.toRadians(Alpha)));
            sendelta = round(Math.sin(Math.toRadians(Delta)));
            ladoB = round((ladoA*senbeta)/senalpha);
            ladoC = round((ladoA*sendelta)/senalpha);
            return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
        }else if (ladoB != 0){
            //B Alpha Beta Delta
            senbeta  = round(Math.sin(Math.toRadians(Beta)));
            senalpha = round(Math.sin(Math.toRadians(Alpha)));
            sendelta = round(Math.sin(Math.toRadians(Delta)));
            ladoA = round((ladoB*senalpha)/senbeta);
            ladoC = round((ladoB*sendelta)/senbeta);
            return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
        }else if (ladoC != 0){
            //C Alpha Beta Delta
            senbeta  = Math.sin(round(Math.toRadians(Beta)));
            senalpha = Math.sin(round(Math.toRadians(Alpha)));
            sendelta = Math.sin(round(Math.toRadians(Delta)));
            ladoA = round((ladoC*senalpha)/sendelta);
            ladoB = round((ladoC*senbeta)/sendelta);
            return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
        }
        return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
    }

    public String[] TresLados(Double ladoA, Double ladoB, Double ladoC, Double Alpha, Double Beta, Double Delta){
        Double ac,bc,cc;
        ac = Math.pow(ladoA,2);
        bc = Math.pow(ladoB,2);
        cc = Math.pow(ladoC,2);
        Alpha = round(Math.toDegrees(Math.acos((ac-bc-cc)/(-2*ladoB*ladoC))));
        Beta =  round(Math.toDegrees(Math.acos((bc-ac-cc)/(-2*ladoA*ladoC))));
        Delta = round(Math.toDegrees(Math.acos((cc-bc-ac)/(-2*ladoB*ladoA))));
        return GuardarArray(ladoA, ladoB ,ladoC, Alpha, Beta, Delta);
    }

    public String[] GuardarArray(Double ladoA, Double ladoB, Double ladoC, Double Alpha, Double Beta, Double Delta){
        String[] v = {f.format(ladoA), f.format(ladoB), f.format(ladoC), f.format(Alpha), f.format(Beta), f.format(Delta)};
        return v;
    }

}
