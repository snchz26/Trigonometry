package animalista.trigonometry.android.trigonometry;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by Eduardo Sanchez on 1/2/2015.
 */
public class Rectangulo extends Fragment {


    public static final DecimalFormat f = new DecimalFormat("#.###", new DecimalFormatSymbols(Locale.US));
    static EditText et_A;
    static EditText et_B;
    static EditText et_C;
    static EditText et_alpha;
    static EditText et_beta;
    static ImageView igual;
    public static TableLayout tl;
    static TextView senAlpha;
    static TextView cosAlpha;
    static TextView tgAlpha;
    static TextView cscAlpha;
    static TextView secAlpha;
    static TextView ctgAlpha;
    static TextView senBeta;
    static TextView cosBeta;
    static TextView tgBeta;
    static TextView cscBeta;
    static TextView secBeta;
    static TextView ctgBeta;

    public Rectangulo() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.rectangulo, container, false);
    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    public String[] Resultado(String et_A, String et_B, String et_C, String et_alpha, String et_beta) {
        String v[] = {et_A, et_B, et_C, et_alpha, et_beta};
        do {
        v = SolvingLoopClass.SolvingLoop(v);
        System.out.println("A:" + v[0] + " B:" + v[1] + " C:" + v[2] + " a:" + v[3] + " b:" + v[4]);
        }while (v[0].matches("") || v[1].matches("") || v[2].matches("") || v[3].matches("") || v[4].matches(""));

        return v;
    }



    public void Clean() {

        et_A.setText("");
        et_A.setEnabled(true);
        et_A.setFocusable(true);
        et_A.setClickable(true);
        et_A.setFocusableInTouchMode(true);

        et_B.setText("");
        et_B.setEnabled(true);
        et_B.setFocusable(true);
        et_B.setClickable(true);
        et_B.setFocusableInTouchMode(true);

        et_C.setText("");
        et_C.setEnabled(true);
        et_C.setFocusable(true);
        et_C.setClickable(true);
        et_C.setFocusableInTouchMode(true);

        et_alpha.setText("");
        et_alpha.setEnabled(true);
        et_alpha.setFocusable(true);
        et_alpha.setClickable(true);
        et_alpha.setFocusableInTouchMode(true);

        et_beta.setText("");
        et_beta.setEnabled(true);
        et_beta.setFocusable(true);
        et_beta.setClickable(true);
        et_beta.setFocusableInTouchMode(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //
        et_A = (EditText) getActivity().findViewById(R.id.et_A);
        et_B = (EditText) getActivity().findViewById(R.id.et_B);
        et_C = (EditText) getActivity().findViewById(R.id.et_C);
       et_alpha = (EditText) getActivity().findViewById(R.id.et_alpha);
        et_beta = (EditText) getActivity().findViewById(R.id.et_beta);

       senAlpha = (TextView) getActivity().findViewById(R.id.senAlpha);
       cosAlpha = (TextView) getActivity().findViewById(R.id.cosAlpha);
        tgAlpha = (TextView) getActivity().findViewById(R.id.tgAlpha);
       cscAlpha = (TextView) getActivity().findViewById(R.id.cscAlpha);
       secAlpha = (TextView) getActivity().findViewById(R.id.secAlpha);
       ctgAlpha = (TextView) getActivity().findViewById(R.id.ctgAlpha);
        senBeta = (TextView) getActivity().findViewById(R.id.senBeta);
        cosBeta = (TextView) getActivity().findViewById(R.id.cosBeta);
         tgBeta = (TextView) getActivity().findViewById(R.id.tgBeta);
        cscBeta = (TextView) getActivity().findViewById(R.id.cscBeta);
        secBeta = (TextView) getActivity().findViewById(R.id.secBeta);
        ctgBeta = (TextView) getActivity().findViewById(R.id.ctgBeta);
          tl = (TableLayout) getActivity().findViewById(R.id.TL);

        igual = (ImageView) getActivity().findViewById(R.id.igual);
        igual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //todos vacios
                if ((et_A.getText().toString().equals("") && et_B.getText().toString().equals("") &&
                        et_C.getText().toString().equals("") && et_alpha.getText().toString().equals("") && et_beta.getText().toString().equals(""))
                        ||//a b c vacios
                        (et_A.getText().toString().equals("") && et_B.getText().toString().equals("") && et_C.getText().toString().equals(""))
                        ||//todos vacios menos uno
                        (!et_A.getText().toString().equals("") && et_B.getText().toString().equals("") &&
                                et_C.getText().toString().equals("") && et_alpha.getText().toString().equals("") && et_beta.getText().toString().equals(""))
                        ||
                        (et_A.getText().toString().equals("") && !et_B.getText().toString().equals("") &&
                                et_C.getText().toString().equals("") && et_alpha.getText().toString().equals("") && et_beta.getText().toString().equals(""))
                        ||
                        (et_A.getText().toString().equals("") && et_B.getText().toString().equals("") &&
                                !et_C.getText().toString().equals("") && et_alpha.getText().toString().equals("") && et_beta.getText().toString().equals(""))
                        ||
                        (et_A.getText().toString().equals("") && et_B.getText().toString().equals("") &&
                                et_C.getText().toString().equals("") && !et_alpha.getText().toString().equals("") && et_beta.getText().toString().equals(""))
                        ||
                        (et_A.getText().toString().equals("") && et_B.getText().toString().equals("") &&
                                et_C.getText().toString().equals("") && et_alpha.getText().toString().equals("") && !et_beta.getText().toString().equals(""))
                        ) {

                    Toast.makeText(Rectangulo.this.getActivity(), "Please check the data", Toast.LENGTH_SHORT).show();
                } else if (!et_A.isEnabled()) {
                    Clean();
                    tl.setVisibility(View.GONE);
                    igual.setImageResource(getImageId(Rectangulo.this.getActivity(), "ic_action_done"));

                } else {
                    String valores[] = Resultado(et_A.getText().toString(), et_B.getText().toString(), et_C.getText().toString(),
                            et_alpha.getText().toString(), et_beta.getText().toString());

                    System.out.println("0: " + valores[0]);
                    System.out.println("1: " + valores[1]);
                    System.out.println("2: " + valores[2]);
                    System.out.println("3: " + valores[3]);
                    System.out.println("4: " + valores[4]);

                    if (Double.isNaN(Double.parseDouble(valores[0])) || Double.isNaN(Double.parseDouble(valores[1]))
                        || Double.isNaN(Double.parseDouble(valores[2])) || Double.isNaN(Double.parseDouble(valores[3]))
                        || Double.isNaN(Double.parseDouble(valores[4]))
                        //A y B Mayor a C
                        || Double.parseDouble(valores[0]) >= Double.parseDouble(valores[2])
                        || Double.parseDouble(valores[1]) >= Double.parseDouble(valores[2])
                        //Cualquiera igual a 0
                        || Double.parseDouble(valores[0]) == 0 || Double.parseDouble(valores[0]) == 0 || Double.parseDouble(valores[0]) == 0
                        || Double.parseDouble(valores[0]) == 0 || Double.parseDouble(valores[0]) == 0
                        //Suma de cuadrados correcta
                        || ((Math.pow(Double.parseDouble(valores[0]),2) + Math.pow(Double.parseDouble(valores[1]),2)) - Math.pow(Double.parseDouble(valores[2]),2) > 1)
                        || ((Math.pow(Double.parseDouble(valores[0]),2) + Math.pow(Double.parseDouble(valores[1]),2)) - Math.pow(Double.parseDouble(valores[2]),2) < -1)
                        //Suma de angulos correcta
                        || (Double.parseDouble(valores[3]) + Double.parseDouble(valores[4])> 90+1)
                        && (Double.parseDouble(valores[3]) + Double.parseDouble(valores[4])< 90-1)
                        ) {
                        Toast.makeText(Rectangulo.this.getActivity(), "Triangle doesn't exist or is not a Square Triangle", Toast.LENGTH_SHORT).show();
                        Clean();
                    } else {
                        et_A.setText(valores[0]);
                        et_A.setEnabled(false);
                        et_A.setTextColor(Color.BLACK);
                        et_A.setFocusable(false);

                        et_B.setText(valores[1]);
                        et_B.setEnabled(false);
                        et_B.setTextColor(Color.BLACK);
                        et_B.setFocusable(false);

                        et_C.setText(valores[2]);
                        et_C.setEnabled(false);
                        et_C.setTextColor(Color.BLACK);
                        et_C.setFocusable(false);

                        et_alpha.setText(valores[3] + "°");
                        et_alpha.setEnabled(false);
                        et_alpha.setTextColor(Color.BLACK);
                        et_alpha.setFocusable(false);

                        et_beta.setText(valores[4] + "°");
                        et_beta.setEnabled(false);
                        et_beta.setTextColor(Color.BLACK);
                        et_beta.setFocusable(false);

                        tl.setVisibility(View.VISIBLE);
                        senAlpha.setText("Sin " + valores[3] + " = " + f.format(round(Math.sin(Math.toRadians(Double.parseDouble(valores[3]))))));
                        cosAlpha.setText("Cos " + valores[3] + " = " + f.format(round(Math.cos(Math.toRadians(Double.parseDouble(valores[3]))))));
                          tgAlpha.setText("Tg " + valores[3] + " = " + f.format(round(Math.tan(Math.toRadians(Double.parseDouble(valores[3]))))));
                    cscAlpha.setText("Csc " + valores[3] + " = " + f.format(1 / round(Math.sin(Math.toRadians(Double.parseDouble(valores[3]))))));
                    secAlpha.setText("Sec " + valores[3] + " = " + f.format(1 / round(Math.cos(Math.toRadians(Double.parseDouble(valores[3]))))));
                    ctgAlpha.setText("Ctg " + valores[3] + " = " + f.format(1 / round(Math.tan(Math.toRadians(Double.parseDouble(valores[3]))))));
                         senBeta.setText("Sin " + valores[4] + " = " + f.format(round(Math.sin(Math.toRadians(Double.parseDouble(valores[4]))))));
                         cosBeta.setText("Cos " + valores[4] + " = " + f.format(round(Math.cos(Math.toRadians(Double.parseDouble(valores[4]))))));
                           tgBeta.setText("Tg " + valores[4] + " = " + f.format(round(Math.tan(Math.toRadians(Double.parseDouble(valores[4]))))));
                     cscBeta.setText("Csc " + valores[4] + " = " + f.format(1 / round(Math.sin(Math.toRadians(Double.parseDouble(valores[4]))))));
                     secBeta.setText("Sec " + valores[4] + " = " + f.format(1 / round(Math.cos(Math.toRadians(Double.parseDouble(valores[4]))))));
                     ctgBeta.setText("Ctg " + valores[4] + " = " + f.format(1 / round(Math.tan(Math.toRadians(Double.parseDouble(valores[4]))))));

                        igual.setImageResource(getImageId(Rectangulo.this.getActivity(), "ic_action_refresh"));
                    }
                }
            }
        });

    }

    public static double round(double paramDouble)
    {
        return Math.round(paramDouble * 10000000000.0D) / 10000000000.0D;
    }

}

final class SolvingLoopClass{
    public static String[] SolvingLoop(String[] v){
        ////////////////////////////////
        //alpha lleno
        if (!v[3].matches("")) {
            v[4] = String.valueOf(Rectangulo.f.format(Rectangulo.round(180 - 90 - Double.parseDouble(v[3]))));

            //a y alpha llenos
            if (!v[0].matches("")) {
                Double numa = Double.parseDouble(v[0]);
                Double numalpha = Double.parseDouble(v[3]);
                v[1] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numa / Math.tan(Math.toRadians(numalpha)))));
                v[2] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numa / Math.sin(Math.toRadians(numalpha)))));
                return v;
            }

            //b y alpha llenos
            else if (!v[1].matches("")) {
                Double numb = Double.parseDouble(v[1]);
                Double numalpha = Double.parseDouble(v[3]);
                v[0] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numb * Math.tan(Math.toRadians(numalpha)))));
                v[2] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numb / Math.cos(Math.toRadians(numalpha)))));
                return v;
            }

            //c y aplha llenos
            else if (!v[2].matches("")) {
                Double numc = Double.parseDouble(v[2]);
                Double numalpha = Double.parseDouble(v[3]);
                v[0] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numc * Math.sin(Math.toRadians(numalpha)))));
                v[1] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numc * Math.cos(Math.toRadians(numalpha)))));
                return v;
            }
            return v;
        }
        //beta lleno
        else if (!v[4].matches("")) {
            v[3] = String.valueOf(Rectangulo.f.format(Rectangulo.round(180 - 90 - Double.parseDouble(v[4]))));

            //a y beta llenos
            if (!v[0].matches("")) {
                Double numa = Double.parseDouble(v[0]);
                Double numbeta = Double.parseDouble(v[4]);
                v[1] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numa * Math.tan(Math.toRadians(numbeta)))));
                v[2] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numa / Math.cos(Math.toRadians(numbeta)))));
                return v;
            }

            //b y beta llenos
            else if (!v[1].matches("")) {
                Double numb = Double.parseDouble(v[1]);
                Double numbeta = Double.parseDouble(v[4]);
                v[0] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numb / Math.tan(Math.toRadians(numbeta)))));
                v[2] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numb / Math.sin(Math.toRadians(numbeta)))));
                return v;
            }

            //c y beta llenos
            else if (!v[2].matches("")) {
                Double numc = Double.parseDouble(v[2]);
                Double numbeta = Double.parseDouble(v[4]);
                v[0] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numc * Math.cos(Math.toRadians(numbeta)))));
                v[1] = String.valueOf(Rectangulo.f.format(Rectangulo.round(numc * Math.sin(Math.toRadians(numbeta)))));
                return v;
            }
            return v;
        }

        //alpha y beta vacios
        if (v[3].matches("") && v[4].matches("")) {

            //a y b llenos
            if (!v[0].matches("") && !v[1].matches("")) {
                Double numb = Double.parseDouble(v[1]);
                Double numa = Double.parseDouble(v[0]);
                v[3] = String.valueOf(Rectangulo.f.format(Rectangulo.round(Math.atan(numa / numb) * 180 / Math.PI)));
                v[4] = String.valueOf(Rectangulo.f.format(Rectangulo.round(Math.atan(numb / numa) * 180 / Math.PI)));
            }
            //a y c llenos
            else if (!v[0].matches("") && !v[2].matches("")) {
                Double numc = Double.parseDouble(v[2]);
                Double numa = Double.parseDouble(v[0]);
                v[3] = String.valueOf(Rectangulo.f.format(Rectangulo.round(Math.asin(numa / numc) * 180 / Math.PI)));
                v[4] = String.valueOf(Rectangulo.f.format(Rectangulo.round(Math.acos(numa / numc) * 180 / Math.PI)));
            }
            //b y c llenos
            else if (!v[1].matches("") && !v[2].matches("")) {
                Double numc = Double.parseDouble(v[2]);
                Double numb = Double.parseDouble(v[1]);
                v[3] = String.valueOf(Rectangulo.f.format(Rectangulo.round(Math.acos(numb / numc) * 180 / Math.PI)));
                v[4] = String.valueOf(Rectangulo.f.format(Rectangulo.round(Math.asin(numb / numc) * 180 / Math.PI)));
            }
        }
        return v;
    }
}